package com.progmatic.tictactoeexam;

import com.progmatic.tictactoeexam.enums.PlayerType;
import com.progmatic.tictactoeexam.exceptions.CellException;
import com.progmatic.tictactoeexam.interfaces.Board;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Vktor
 */
public class Game implements Board {

    private final Cell[][] ticTacBoard = new Cell[3][3];

    public Cell[][] getBoard() {
        return ticTacBoard;
    }

    @Override
    public PlayerType getCell(int rowIdx, int colIdx) throws CellException {
        Cell actCell = new Cell(rowIdx, colIdx);
        rowIdx = actCell.getRow();
        colIdx = actCell.getCol();
        if (rowIdx != 0 && rowIdx != 1 && rowIdx != 2) {
            String message = "Your pick is off the board pal.";
            throw new CellException(rowIdx, colIdx, message);
        }
        if (colIdx != 0 || colIdx != 1 || colIdx != 2) {
            String message = "Your pick is off the board pal.";
            throw new CellException(rowIdx, colIdx, message);
        }
        return actCell.getCellsPlayer();
    }

    @Override
    public void put(Cell cell) throws CellException {
        int rowIdx = cell.getRow();
        int colIdx = cell.getCol();
        if (rowIdx != 0 || rowIdx != 1 && rowIdx != 2 && colIdx != 0 && colIdx != 1 && colIdx != 2) {
            String message = "You're off the board.";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        if (cell.getCellsPlayer() != PlayerType.EMPTY) {
            String message = "This cell is aleady occupied.";
            throw new CellException(cell.getRow(), cell.getCol(), message);
        }
        PlayerType actPT = cell.getCellsPlayer();
    }

    @Override
    public boolean hasWon(PlayerType p) {
        boolean result = false;
        fillBoard();
        int rowIdx;
        int colIdx;
        for (rowIdx = 0; rowIdx <= 2; rowIdx++) {
            if (ticTacBoard[rowIdx][0].getCellsPlayer() == ticTacBoard[rowIdx][1].getCellsPlayer()
                    && ticTacBoard[rowIdx][0].getCellsPlayer() == ticTacBoard[rowIdx][2].getCellsPlayer()) {
                result = true;
            }
        }
        for (colIdx = 0; colIdx <= 2; colIdx++) {
            if (ticTacBoard[0][colIdx].getCellsPlayer() == ticTacBoard[1][colIdx].getCellsPlayer()
                    && ticTacBoard[0][colIdx].getCellsPlayer() == ticTacBoard[2][colIdx].getCellsPlayer()) {
                result = true;
            }
        }
        if (ticTacBoard[0][0].getCellsPlayer() == ticTacBoard[1][1].getCellsPlayer()
                && ticTacBoard[0][0].getCellsPlayer() == ticTacBoard[2][2].getCellsPlayer()) {
            result = true;
        }
        if (ticTacBoard[0][2].getCellsPlayer() == ticTacBoard[1][1].getCellsPlayer()
                && ticTacBoard[1][1].getCellsPlayer() == ticTacBoard[2][0].getCellsPlayer()) {
            result = true;
        }
        return result;
    }

    public Cell[][] fillBoard() {
        int rowIdx;
        int colIdx;
        for (rowIdx = 0; rowIdx <= 2; rowIdx++) {
            for (colIdx = 0; colIdx <= 2; colIdx++) {
                ticTacBoard[rowIdx][colIdx] = new Cell(rowIdx, colIdx);
            }
        }
        return ticTacBoard;
    }

    @Override
    public List<Cell> emptyCells() {
        fillBoard();
        List<Cell> result = new ArrayList<>();
        int rowIdx;
        int colIdx;
        for (rowIdx = 0; rowIdx <= 2; rowIdx++) {
            for (colIdx = 0; colIdx <= 2; colIdx++) {
                if (ticTacBoard[rowIdx][colIdx].getCellsPlayer() == PlayerType.EMPTY) {
                    result.add(ticTacBoard[rowIdx][colIdx]);
                }
            }
        }
        return result;

    }
}
